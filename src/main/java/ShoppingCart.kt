import java.util.*

class ShoppingCart {
    private val books = ArrayList<Int>()
    private val items = HashMap<Int, Int>()

    fun addBooks(books: ArrayList<Int>) {
        this.books.addAll(books)
    }

    fun getPrice(): Double {
        var price = 0.0

        this.books.forEach {
            if (!items.containsKey(it)) {
                items.put(it, 1)
            } else {
                items[it]!!.plus(1)
                price += 8
            }
        }

        return getPriceWithDiscount(price)
    }

    private fun getDiscount(items: Int): Double {
        return when (items) {
            2 -> 0.95
            3 -> 0.90
            4 -> 0.80
            5 -> 0.75
            else -> 1.0
        }
    }

    private fun getPriceWithDiscount(price: Double): Double {
        var priceWithDiscount = price
        val discount = getDiscount(items.size)
        priceWithDiscount += items.size * 8 * discount

        return priceWithDiscount
    }
}
