import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.*

class ShoppingCartTest {
    lateinit var shoppingCart: ShoppingCart;

    @BeforeEach
    fun setup() {
        shoppingCart = ShoppingCart()
    }

    @Test
    fun `one book cost 8€`() {
        val books = arrayListOf<Int>(1)
        shoppingCart.addBooks(books)

        Assertions.assertEquals(8.0, shoppingCart.getPrice())
    }

    @Test
    fun `two same books cost 16€`() {
        val books = arrayListOf<Int>(1, 1)
        shoppingCart.addBooks(books)

        Assertions.assertEquals(16.0, shoppingCart.getPrice())
    }

    @Test
    fun `two different books give 5% discount`() {
        val books = arrayListOf<Int>(1, 2)
        shoppingCart.addBooks(books)

        Assertions.assertEquals((2 * 8) * 0.95, shoppingCart.getPrice())
    }

    @Test
    fun `three different books give 10% discount`() {
        val books = arrayListOf<Int>(1, 2, 3)
        shoppingCart.addBooks(books)

        Assertions.assertEquals((3 * 8) * 0.90, shoppingCart.getPrice())
    }

    @Test
    fun `four different books give 20% discount`() {
        val books = arrayListOf<Int>(1, 2, 3, 4)
        shoppingCart.addBooks(books)

        Assertions.assertEquals((4 * 8) * 0.80, shoppingCart.getPrice())
    }

    @Test
    fun `five different books give 25% discount`() {
        val books = arrayListOf<Int>(1, 2, 3, 4, 5)
        shoppingCart.addBooks(books)

        Assertions.assertEquals((5 * 8) * 0.75, shoppingCart.getPrice())
    }

    @Test
    fun `five different books and three same books cost 54€`() {
        val books = arrayListOf<Int>(1, 2, 3, 4, 5, 1, 2, 3)
        shoppingCart.addBooks(books)

        Assertions.assertEquals((5 * 8) * 0.75 + 3 * 8, shoppingCart.getPrice())
    }
}